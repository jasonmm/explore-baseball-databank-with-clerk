;; # Team Historical OPS
^{:nextjournal.clerk/visibility {:code :hide}}
(ns team-historical-ops
  (:require
    [nextjournal.clerk :as clerk]
    [viewers.select-input :refer [select-input]]
    [utils]))

(def franchises
  (utils/query-results "https://baseballdb.lawlesst.net/chadwick.json?sql=select+franchID%2C+franchName+from+TeamsFranchises+where+active%3D%27Y%27+order+by+franchID+asc"))

(def franchise-options (map (fn [[id n]] {:value id :label n}) franchises))

(def mlb-ops-by-year
  (utils/query-results "https://baseballdb.lawlesst.net/chadwick.json?sql=select%0D%0A++b.yearID%2C%0D%0A++cast(%0D%0A++++(sum(b.H)+%2B+sum(b.BB)+%2B+sum(coalesce(b.HBP%2C+0)))+as+real%0D%0A++)+%2F(%0D%0A++++sum(b.AB)+%2B+sum(b.BB)+%2B+sum(coalesce(b.HBP%2C+0))+%2B+sum(coalesce(b.SF%2C+0))%0D%0A++)+%2B+(%0D%0A++++(sum(b.H)+-+sum(b.%5B2B%5D)+-+sum(b.%5B3B%5D)+-+sum(b.HR))+%2B+(2+*+sum(b.%5B2B%5D))+%2B+(3+*+sum(b.%5B3B%5D))+%2B+(4+*+sum(b.HR))%0D%0A++)+%2F+cast(sum(b.AB)+as+real)+as+ops%0D%0Afrom%0D%0A++Batting+b%0D%0Awhere+b.yearID%3E1902%0D%0Agroup+by+b.yearID%0D%0Aorder+by+b.yearID"))

;; ## Select Team

;; Choose the team from the dropdown.

^{::clerk/viewer     (select-input franchise-options)
  ::clerk/visibility {:code :hide}}
(defonce selected-franchise-id (atom nil))

@selected-franchise-id

(def ops-url
  "https://baseballdb.lawlesst.net/chadwick.json?sql=select%0D%0A++b.teamID%2C%0D%0A++b.yearID%2C%0D%0A++t.name%2C%0D%0A++f.franchName%2C%0D%0A++cast(%0D%0A++++(sum(b.H)+%2B+sum(b.BB)+%2B+sum(coalesce(b.HBP%2C+0)))+as+real%0D%0A++)+%2F(%0D%0A++++sum(b.AB)+%2B+sum(b.BB)+%2B+sum(coalesce(b.HBP%2C+0))+%2B+sum(coalesce(b.SF%2C+0))%0D%0A++)+%2B+(%0D%0A++++(sum(b.H)+-+sum(b.[2B])+-+sum(b.[3B])+-+sum(b.HR))+%2B+(2+*+sum(b.[2B]))+%2B+(3+*+sum(b.[3B]))+%2B+(4+*+sum(b.HR))%0D%0A++)+%2F+cast(sum(b.AB)+as+real)+as+ops%0D%0Afrom%0D%0A++Batting+b%0D%0A++inner+join+Teams+t+on+b.teamID+%3D+t.teamID%0D%0A++and+b.yearID+%3D+t.yearID%0D%0A++inner+join+TeamsFranchises+f+on+t.franchID+%3D+f.franchID%0D%0Awhere%0D%0A++b.yearID+%3E+1902+AND+f.franchID+%3D+:franchiseid%0D%0Agroup+by%0D%0A++b.teamID%2C%0D%0A++b.yearID+order+by+b.yearID+asc&franchiseid=")

(def ops-data-keys
  "Must be in the same order as the SELECT clause in the `ops-url` query."
  [:team-id :year :team-name :franchise-name :ops])

(def ops-data
  (map #(zipmap ops-data-keys %)
       (utils/query-results (str ops-url @selected-franchise-id))))
(def first-year (-> ops-data first :year))
(def last-year (-> ops-data last :year))
(def mlb-for-team-years
  (filter #(<= first-year (first %) last-year)
          mlb-ops-by-year))

(def franchise-name (-> ops-data last :franchise-name))

(def plot-data
  [{:x    (map :year ops-data)
    :y    (map :ops ops-data)
    :type "scatter"
    :name franchise-name}
   {:x    (map first mlb-for-team-years)
    :y    (map second mlb-for-team-years)
    :type "scatter"
    :name "MLB"}])

(clerk/plotly {:data   plot-data
               :layout {:title (str "Historical OPS For " franchise-name)
                        :xaxis {:title "Year"}
                        :yaxis {:title "OPS"}}
               :config {:staticPlot  false
                        :displayLogo false}})
