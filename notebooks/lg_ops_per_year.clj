;; # League OPS Throughout The Years
^{:nextjournal.clerk/visibility {:code :hide}}
(ns lg-ops-per-year
  (:require
    [nextjournal.clerk :as clerk]
    [cheshire.core :as json]))

(comment
  ^{:nextjournal.clerk/visibility {:code :hide :result :hide}}
  (clerk/serve! {:watch-paths ["notebooks" "src"]}))

;; Query an [online Baseball Databank database](https://baseballdb.lawlesst.net)
;; to get the OPS for each league and year. The formatted query can be seen
;; [here](https://baseballdb.lawlesst.net/chadwick?sql=select%0D%0A++yearID%2C%0D%0A++lgID%2C%0D%0A++cast(%0D%0A++++(sum(H)+%2B+sum(BB)+%2B+sum(coalesce(HBP%2C+0)))+as+real%0D%0A++)+%2F(%0D%0A++++sum(AB)+%2B+sum(BB)+%2B+sum(coalesce(HBP%2C+0))+%2B+sum(coalesce(SF%2C+0))%0D%0A++)+%2B+(%0D%0A++++(sum(H)+-+sum([2B])+-+sum([3B])+-+sum(HR))+%2B+(2+*+sum([2B]))+%2B+(3+*+sum([3B]))+%2B+(4+*+sum(HR))%0D%0A++)+%2F+cast(sum(AB)+as+real)+as+ops%0D%0Afrom%0D%0A++Batting%0D%0Awhere%0D%0A++yearID+%3E+1902%0D%0Agroup+by%0D%0A++yearID%2C%0D%0A++lgID%0D%0Aorder+by%0D%0A++yearID).

(def query-results
  (-> "https://baseballdb.lawlesst.net/chadwick.json?sql=select%0D%0A++yearID%2C%0D%0A++lgID%2C%0D%0A++cast((sum(H)+%2B+sum(BB)+%2B+sum(coalesce(HBP%2C+0)))+as+real)+%2F(%0D%0A++++sum(AB)+%2B+sum(BB)+%2B+sum(coalesce(HBP%2C+0))+%2B+sum(coalesce(SF%2C+0))%0D%0A++)+%2B%0D%0A++(%0D%0A++++(sum(H)+-+sum(%5B2B%5D)+-+sum(%5B3B%5D)+-+sum(HR))+%2B+(2+*+sum(%5B2B%5D))+%2B+(3+*+sum(%5B3B%5D))+%2B+(4+*+sum(HR))%0D%0A++)+%2F+cast(sum(AB)+as+real)+as+ops%0D%0Afrom%0D%0A++Batting%0D%0Awhere%0D%0A++yearID+%3E+1902%0D%0Agroup+by%0D%0A++yearID%2C%0D%0A++lgID%0D%0Aorder+by%0D%0A++yearID"
      slurp
      (json/parse-string true)
      :rows))

^{:nextjournal.clerk/visibility {:result :hide}}
(defn third [coll] (nth coll 2))

(def plot-data
  (->> query-results
       (group-by second)
       (map (fn [[lg data]]
              {:x    (map first data)
               :y    (map third data)
               :type "scatter"
               :name lg}))))

(clerk/plotly {:data   plot-data
               :layout {:title "League OPS Per Year"
                        :xaxis {:title "Year"}
                        :yaxis {:title "OPS"}}
               :config {:staticPlot  false
                        :displayLogo false}})

