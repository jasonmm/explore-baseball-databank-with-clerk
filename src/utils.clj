(ns utils
  (:require [cheshire.core :as json]))

(defn query-results
  "Return result rows for the given JSON query URL."
  [url]
  (-> url
      slurp
      (json/parse-string true)
      :rows))
