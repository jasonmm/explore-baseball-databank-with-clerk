(ns viewers.text-input
  "A Clerk viewer for text input. Copied from https://github.com/nextjournal/clerk-demo/blob/cb212b2edc64e762bcd1af56417ef9f6b2fde5b1/notebooks/controls.clj

  Usage:
  ^{::clerk/viewer text-input}
  (defonce text-state (atom \"\"))

  The value of `text-state` will contain what the user types."
  (:require [nextjournal.clerk :as clerk]))

(def text-input
  ;; We assign a predicate (i.e. `:pred`) function for this viewer,
  ;; which in this case just checks to see if there's a `var-from-def`
  ;; (that is, the form Clerk is evaluating defines a var).
  {:pred         ::clerk/var-from-def

   ;; When we specify a `:transform-fn`, it gets run on the JVM side
   ;; to pre-process our value before sending it to the front-end. In
   ;; this case we want to send the symbol for the var along with the
   ;; unwrapped value because our custom renderer will need to know
   ;; both of those things (see below).
   ;;
   ;; Normally, Clerk's front-end is very careful to fetch data from
   ;; the JVM is bite-sized chunks to avoid killing the browser. But
   ;; sometimes we need to override that mechanism, which is done by
   ;; calling `clerk/mark-presented` to ask Clerk to send the whole
   ;; value as-is to the browser.
   :transform-fn (comp clerk/mark-presented
                       (clerk/update-val (fn [{::clerk/keys [var-from-def]}]
                                           {:var-name (symbol var-from-def) :value @@var-from-def})))

   ;; The `:render-fn` is the heart of any viewer. It will be executed
   ;; by a ClojureScript runtime in the browser, so — unlike these
   ;; other functions — we must quote it for transmission over the
   ;; wire.
   ;;
   ;; The interactive two-way binding magic comes from using Clerk's
   ;; support for Hiccup to produce an input tag whose `:on-input`
   ;; handler calls `clerk-eval` to send a quoted form back to the
   ;; JVM. This ability to easily transmit arbitrary code between the
   ;; front- and back- ends of the system is extremely
   ;; powerful — thanks, Lisp!
   :render-fn    '(fn [{:keys [var-name value]}]
                    (v/html [:input {:type          :text
                                     :placeholder   "⌨️"
                                     :initial-value value
                                     :class         "px-3 py-3 placeholder-blueGray-300 text-blueGray-600 relative bg-white bg-white rounded text-sm border border-blueGray-300 outline-none focus:outline-none focus:ring w-full"
                                     :on-input      #(v/clerk-eval `(reset! ~var-name ~(.. % -target -value)))}]))})
