(ns viewers.select-input
  "A Clerk viewer for a dropdown.

  Usage:
  ^{::clerk/viewer (select-input options)}
  (defonce selected-value (atom nil))

  `options` is a collection of map with `:value` and `:label` keys.
  The value of `selected-value` will contain the `:value` of the selected option."
  (:require [nextjournal.clerk :as clerk]))

(defn select-input
  [options]
  {:pred         ::clerk/var-from-def
   :transform-fn (comp clerk/mark-presented
                       (clerk/update-val
                         (fn [{::clerk/keys [var-from-def]}]
                           {:var-name (symbol var-from-def)
                            :value    @@var-from-def
                            :options  options})))
   :render-fn    '(fn [{:keys [var-name value options]}]
                    (v/html [:select {:value     value
                                      :class     "px-3 py-3 text-blueGray-600 relative bg-white bg-white rounded-lg border border-blueGray-300 outline-none focus:outline-none w-full"
                                      :on-change #(v/clerk-eval `(reset! ~var-name ~(.. % -target -value)))}
                             (for [o options]
                               ^{:key (:value 0)}
                               [:option {:value (:value o)} (:label o)])]))})

