# Explore Baseball Databank

Contains [Clerk](https://github.com/nextjournal/clerk) notebooks for exploring
Baseball Databank data.

# Development

1. Start a REPL
2. Eval `(clerk/serve! {:watch-paths ["notebooks" "src"]})`
3. Eval `(clerk/show! "path-to-file")`

# Release

Run `clj -X:clerk-cd`. Output files will be placed in the _public/_ directory.
